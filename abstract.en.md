Proof assistants are cutting-edge research software that let users describe mathematical proofs in a programming-like language, and can verify that those proofs are correct. The computations required are time- and memory-intensive.

We propose to work on the Coq proof assistant and its implementation langage, OCaml. We want to jointly improve Coq and OCaml by studying the Coq implementation to understand which features to add to OCaml that could improve Coq and other software in this problem domain. (Details: improve the flambda2 optimizer, extend the runtime system with an isolated, immutable, design an untyped input langage for the OCaml compiler for the purposes of Coq xtraction.)

Our consortium has both experts of the Coq implementation and experts of the OCaml implementation, it is designed to have them collaborate together.
